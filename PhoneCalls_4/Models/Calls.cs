using System;

namespace PhoneCalls_4.Models
{
    public class Calls
    {
        public int id { get; set; }
        public Persons user { get; set; }
        public DateTime date_of_calls { get; set; }
        public float duration { get; set; }
        public City city { get; set; }
    }
}